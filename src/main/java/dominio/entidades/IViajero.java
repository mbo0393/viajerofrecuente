package dominio.entidades;

public interface IViajero {

    boolean acumularMillas();
    Integer obtenerMillas();
}
