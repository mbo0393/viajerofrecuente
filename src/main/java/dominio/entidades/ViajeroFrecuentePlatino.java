package dominio.entidades;

public class ViajeroFrecuentePlatino extends ViajeroFrecuente {

    public ViajeroFrecuentePlatino(String id, String nombre, String apellido, Integer numeroMillasDisponibles) {
        super(id, nombre, apellido, numeroMillasDisponibles);
    }

    @Override
    public boolean acumularMillas() {
        this.setNumeroMillasDisponibles(1000 + this.getNumeroMillasDisponibles());
        return true;
    }

    @Override
    public Integer obtenerMillas() {
        return this.getNumeroMillasDisponibles();
    }
}