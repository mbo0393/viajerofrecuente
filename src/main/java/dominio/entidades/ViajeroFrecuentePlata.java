package dominio.entidades;

public class ViajeroFrecuentePlata extends ViajeroFrecuente {

    public ViajeroFrecuentePlata(String id, String nombre, String apellido, Integer numeroMillasDisponibles) {
        super(id, nombre, apellido, numeroMillasDisponibles);
    }

    @Override
    public boolean acumularMillas() {
        this.setNumeroMillasDisponibles(200 + this.getNumeroMillasDisponibles());
        return true;
    }

    @Override
    public Integer obtenerMillas() {
        return this.getNumeroMillasDisponibles();
    }
}
