package dominio.entidades.repositorio;

import dominio.entidades.IViajero;

public interface ViajeroFrecuenteRepository {

    IViajero actualizarViajeroFrecuente(IViajero viajero);
}
