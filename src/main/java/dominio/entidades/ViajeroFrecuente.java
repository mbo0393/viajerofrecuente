package dominio.entidades;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
abstract class ViajeroFrecuente implements IViajero {

    private String id;
    private String nombre;
    private String apellido;
    private Integer numeroMillasDisponibles;
}
