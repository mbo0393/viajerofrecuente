package dominio.entidades;

public class ViajeroFrecuenteDorado extends ViajeroFrecuente {

    public ViajeroFrecuenteDorado(String id, String nombre, String apellido, Integer numeroMillasDisponibles) {
        super(id, nombre, apellido, numeroMillasDisponibles);
    }

    @Override
    public boolean acumularMillas() {
        this.setNumeroMillasDisponibles(500 + this.getNumeroMillasDisponibles());
        return true;
    }

    @Override
    public Integer obtenerMillas() {
        return this.getNumeroMillasDisponibles();
    }
}
