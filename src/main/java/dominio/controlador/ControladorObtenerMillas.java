package dominio.controlador;

import dominio.entidades.IViajero;

public class ControladorObtenerMillas {

    public Integer obtenerMillas(IViajero viajero) {
        return viajero.obtenerMillas();
    }
}
