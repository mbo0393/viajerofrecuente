package dominio.controlador;

import dominio.entidades.IViajero;
import dominio.entidades.repositorio.ViajeroFrecuenteRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ControladorPersistirViajeroFrecuente {

    private final ViajeroFrecuenteRepository viajeroFrecuenteRepository;

    public IViajero persistirViajero(IViajero viajero) {
        return viajeroFrecuenteRepository.actualizarViajeroFrecuente(viajero);
    }

}
