package dominio.controlador;

import dominio.entidades.IViajero;

public class ControladorAcumularMillas {

    public boolean acumularMillas(IViajero viajero) {
        return viajero.acumularMillas();
    }
}
