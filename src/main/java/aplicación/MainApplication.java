package aplicación;

import dominio.controlador.ControladorAcumularMillas;
import dominio.controlador.ControladorObtenerMillas;
import dominio.controlador.ControladorPersistirViajeroFrecuente;
import dominio.entidades.IViajero;
import dominio.entidades.ViajeroFrecuenteDorado;
import dominio.entidades.ViajeroFrecuentePlata;
import dominio.entidades.ViajeroFrecuentePlatino;
import infraestructura.adaptadores.mysql.MySqlAdapter;

import java.util.Arrays;
import java.util.List;

public class MainApplication {

    private static final MySqlAdapter mySqlAdapter = new MySqlAdapter();

    private static final ControladorAcumularMillas controladorAcumularMillas = new ControladorAcumularMillas();
    private static final ControladorObtenerMillas controladorObtenerMillas = new ControladorObtenerMillas();
    private static final ControladorPersistirViajeroFrecuente controladorPersistirViajero =
            new ControladorPersistirViajeroFrecuente(mySqlAdapter);

    public static void main(String[] args) {
        List<IViajero> listaViajeros = Arrays.asList(
                new ViajeroFrecuentePlatino("3", "José", "Villareal", 3000),
                new ViajeroFrecuenteDorado("2", "Oscar", "Soto", 500),
                new ViajeroFrecuentePlata("1", "Sebastián", "Ospina", 200)
        );

        obtenerMillas(listaViajeros);
        acumularMillas(listaViajeros);
        persistirEnBD(listaViajeros);
    }

    private static void acumularMillas(List<IViajero> listaViajeros) {
        listaViajeros.forEach(controladorAcumularMillas::acumularMillas);
    }

    private static void obtenerMillas(List<IViajero> listaViajeros) {
        listaViajeros.forEach(viajero ->
                System.out.println(controladorObtenerMillas.obtenerMillas(viajero)));
    }

    private static void persistirEnBD(List<IViajero> listaViajeros) {
        listaViajeros.forEach(viajero -> {
            System.out.println("Viajero actaulziado en la base de datos: " +
                    controladorPersistirViajero.persistirViajero(viajero).toString());
        });
    }
}
